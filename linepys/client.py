# -*- coding: utf-8 -*-
from akad.ttypes import Message
from .api import LineApi
from .models import LineModels
from .timeline import LineTimeline
from random import randint
from datetime import datetime, timedelta, date
from bs4 import BeautifulSoup
from wikiapi import WikiApi
import time,random,sys,json,codecs,urllib,urllib3,requests,threading,glob,os,subprocess,multiprocessing,re,ast,shutil,calendar,tempfile,string,six,timeago
with open('1.json', 'r') as fp:
    wait = json.load(fp)
with open('2.json', 'r') as fp:
    wait2 = json.load(fp)

def loggedIn(func):
    def checkLogin(*args, **kwargs):
        if args[0].isLogin:
            return func(*args, **kwargs)
        else:
            args[0].callback.other("You must login to LINE")
    return checkLogin

class LineClient(LineApi, LineModels, LineTimeline):

    def __init__(self, id=None, passwd=None, authToken=None, certificate=None, systemName=None, showQr=False, appName=None, phoneName=None, keepLoggedIn=True):
        
        LineApi.__init__(self)

        if not (authToken or id and passwd):
            self.qrLogin(keepLoggedIn=keepLoggedIn, systemName=systemName, appName=appName, showQr=showQr)
        if authToken:
            if appName:
                appOrPhoneName = appName
            elif phoneName:
                appOrPhoneName = phoneName
            self.tokenLogin(authToken=authToken, appOrPhoneName=appName)
        if id and passwd:
            self.login(_id=id, passwd=passwd, certificate=certificate, systemName=systemName, phoneName=phoneName, keepLoggedIn=keepLoggedIn)

        self._messageReq = {}
        self.profile    = self._client.getProfile()
        self.groups     = self._client.getGroupIdsJoined()

        LineModels.__init__(self)

    """User"""

    @loggedIn
    def getProfile(self):
        return self._client.getProfile()

    @loggedIn
    def getSettings(self):
        return self._client.getSettings()

    @loggedIn
    def getUserTicket(self):
        return self._client.getUserTicket()

    @loggedIn
    def updateProfile(self, profileObject):
        return self._client.updateProfile(0, profileObject)

    @loggedIn
    def updateSettings(self, settingObject):
        return self._client.updateSettings(0, settingObject)

    @loggedIn
    def updateProfileAttribute(self, attrId, value):
        return self._client.updateProfileAttribute(0, attrId, value)

    """Operation"""

    @loggedIn
    def fetchOperation(self, revision, count):
        return self._client.fetchOperations(revision, count)

    @loggedIn
    def getLastOpRevision(self):
        return self._client.getLastOpRevision()

    def sendMessages(self, messageObject):
       return self._client.sendMessages(0, messageObject)

    """Message"""

    @loggedIn
    def sendMessage(self, to, text, contentMetadata={}, contentType=0):
        msg = Message()
        msg.to, msg._from = to, self.profile.mid
        msg.text = text
        msg.contentType, msg.contentMetadata = contentType, contentMetadata
        if to not in self._messageReq:
            self._messageReq[to] = -1
        self._messageReq[to] += 1
        return self._client.sendMessage(self._messageReq[to], msg)
    @loggedIn
    def lyric(self,to,text):
        try:
            r = requests.get("https://api.genius.com/search?q="+text+"&access_token=2j351ColWKXXVxq1PdUNXDYECI2x4zClLyyAJJkrIeX8K7AQ0F-HTmWfG6tNVszO")
            data = r.json()
            hits = data["response"]["hits"][0]["result"]["api_path"]
            title= "\nTitle: "+data["response"]["hits"][0]["result"]["title"]
            oleh = "\nArtis: "+data["response"]["hits"][0]["result"]["primary_artist"]["name"]
            g = data["response"]["hits"][0]["result"]['song_art_image_thumbnail_url']
            r1 = requests.get("https://api.genius.com"+hits+"?&access_token=2j351ColWKXXVxq1PdUNXDYECI2x4zClLyyAJJkrIeX8K7AQ0F-HTmWfG6tNVszO")
            data1 = r1.json()
            path = data1["response"]["song"]["path"]
            release = data1["response"]["song"]["release_date"]
            page_url = "http://genius.com" + path
            page = requests.get(page_url)
            html = BeautifulSoup(page.text, "html.parser")
            [h.extract() for h in html('script')]
            lyrics = html.find("div", class_="lyrics").get_text()
            self.sendImageWithURL(to,g)
            return self.sendMessage(to," 「 Lyric 」"+title+oleh+lyrics)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def killbl(self,to):
        group = self.getGroup(to)
        gMembMids = [contact.mid for contact in group.kitsunemembers]
        aditya = []
        for a in wait["blacklist"]:
            aditya+=filter(lambda str: str == a, gMembMids)
        if aditya == []:
            self.sendMessage(to,"Nothing Blacklist In This Groups")
        for jj in aditya:
            return self.kickoutFromGroup(to,[jj])
    def blockmention(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Friendlist 」\nFriend List: %i Target' % len(dataMid)
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def sendSticker(self, to, packageId, stickerId):
        contentMetadata = {'STKVER': '1','STKPKGID': packageId,'STKID': stickerId}
        return self.sendMessage(to, '', contentMetadata, 7)
    @loggedIn
    def invitingContact(self,to,mid):
        zx = ""
        zxc = " 「 Invite 」\nType: Contact♪\nTarget: ・"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        zxc +="\nStatus: Success Invite contact♪"
        text = zxc
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        self.findAndAddContactsByMid(mid)
        self.inviteIntoGroup(to, [mid])
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def addsContact(self,to,mid):
        self.findAndAddContactsByMid(mid)
        zx = ""
        zxc = " 「 Add Contact 」\n- Target: ・"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        zxc +="\n- Status: Success Add contact♪"
        text = zxc
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def stealcover(self,to,mid):
        cu = self.getProfileCoverURL(mid)
        path = str(cu)
        try:
            self.sendImageWithURL(to,path)
            zx = ""
            zxc = " 「 Get Cover 」\n- Target: ・"
            zx2 = []
            pesan2 = "@a"" "
            xlen = str(len(zxc))
            xlen2 = str(len(zxc)+len(pesan2)-1)
            zx = {'S':xlen, 'E':xlen2, 'M':mid}
            zx2.append(zx)
            zxc += pesan2
            zxc +="\n- Status: Success Get cover♪"
            text = zxc
            contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
            self.sendMessage(to, text, contentMetadata)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def about(self,to,mid):
        today = date.today()
        future = date(2018,11,30)
        days = (str(future - today))
        comma = days.find(",")
        days = days[:comma]
        zx = ""
        zxc = " 「 About 」\nKitsune 'Self' Edition♪\n「 Subscription 」\nName: "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        zxc +="\nExpired: "+str(future) + "\nIn days: " + days + "\nKey:"+wait["setkey"] + "\n\nAuthor:"
        text = zxc
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        self.sendMessage(to, text, contentMetadata)
        return self.sendContact(to,'uc11acad2da3f37a2b64e2452cbbca2c5')
    @loggedIn
    def me(self,to,mid):
        self.sendContact(to,mid)
        zx = ""
        zxc = " 「 Auto Respon 」\n・ "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def stealpp(self,to,mid):
        try:
            contact = self.getGroup(mid)
        except:
            contact = self.getContact(mid)
        try:
            try:
                cu = "http://dl.profile.line.naver.jp"+ contact.picturePath + "/vp.small"
                self.sendVideoWithURL(to,cu)
                zx = ""
                zxc = " 「 Get Video Picture 」\n- Target: ・"
                zx2 = []
                pesan2 = "@a"" "
                xlen = str(len(zxc))
                xlen2 = str(len(zxc)+len(pesan2)-1)
                zx = {'S':xlen, 'E':xlen2, 'M':mid}
                zx2.append(zx)
                zxc += pesan2
                zxc +="\n- Status: Success Get Video picture♪"
                text = zxc
                contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
                self.sendMessage(to, text, contentMetadata)
            except:
                cu = "http://dl.profile.line-cdn.net/" + contact.kitsunephotoStatus
                self.sendImageWithURL(to,cu)
                zx = ""
                zxc = " 「 Get Picture 」\n- Target: ・"
                zx2 = []
                pesan2 = "@a"" "
                xlen = str(len(zxc))
                xlen2 = str(len(zxc)+len(pesan2)-1)
                zx = {'S':xlen, 'E':xlen2, 'M':mid}
                zx2.append(zx)
                zxc += pesan2
                zxc +="\n- Status: Success Get picture♪"
                text = zxc
                contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
                self.sendMessage(to, text, contentMetadata)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def gettimeline(self,to,mid):
        data = self.getHomeProfile(mid)
        if data['result'] != []:
            try:
                no = 0
                a = " 「 Timeline Results 」\nPenulis: "+str(data['result']['homeInfo']['userInfo']['nickname'])
                for i in data['result']['feeds']:
                    no += 1
                    gtime = i['post']['postInfo']['createdTime']
                    a +="\n" + str(no) + ". Status : "+str(i['post']['contents']['text'])
                    a +="\nTotal Like: "+str(i['post']['postInfo']['likeCount'])
                    a +="\nTotal Comment: "+str(i['post']['postInfo']['commentCount'])
                    a +="\nCreated at: "+str(timeago.format(datetime.now(),gtime/1000)) + "\n"
                a +="\n\nTotal Status: "+str(data['result']['homeInfo']['postCount'])
                a += "\nType %sget timeline ~「number」" %(wait["setkey"])
                return self.sendMessage(to,a)
            except Exception as e:
                return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def mytimeline(self,to,mid):
        data = self.getHomeProfile(mid)
        if data['result'] != []:
            try:
                no = 0
                a = " 「 Timeline Results 」\nPenulis: "+str(data['result']['homeInfo']['userInfo']['nickname'])
                for i in data['result']['feeds']:
                    no += 1
                    gtime = i['post']['postInfo']['createdTime']
                    a +="\n" + str(no) + ". Status : "+str(i['post']['contents']['text'])
                    a +="\nTotal Like: "+str(i['post']['postInfo']['likeCount'])
                    a +="\nTotal Comment: "+str(i['post']['postInfo']['commentCount'])
                    a +="\nCreated at: "+str(timeago.format(datetime.now(),gtime/1000)) + "\n"
                a +="\n\nTotal Status: "+str(data['result']['homeInfo']['postCount'])
                a += "\nType %smytimeline ~「number」" %(wait["setkey"])
                return self.sendMessage(to,a)
            except Exception as e:
                return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def keeploli(self,to,text,mid):
        r = requests.get("http://api.ntcorp.us/storage/search/"+text)
        data = r.text
        data = json.loads(data)
        if data["result"]["ids"] != []:
            no = 0
            a = " 「 Keep 」"
            for music in data["result"]["ids"]:
                no += 1
                a += "\n   " + str(no) + ". api.ntcorp.us/storage/get/" + music
            a += "\nUsage:%s keep %s|num" %(wait["setkey"], str(text))
            self.sendMessage(to,a)
        else:
            return self.taagsaya(to,"Type: Keep Image",mid,str(text)+" not found")
    @loggedIn
    def keeplolidata(self,to,text,mid,pl=''):
        r = requests.get("http://api.ntcorp.us/storage/search/"+text)
        data = r.text
        data = json.loads(data)
        try:
            self.jangantag(to,' 「 Keep 」\nStatus: Waiting...','Type: Keep Image',mid,"Request: Get  "+text)
            self.sendImageWithURL(to,"http://api.ntcorp.us/storage/get/" + data["result"]["ids"][pl-1])
        except Exception as e:
            return self.taagsaya(to,"Type: Keep Image",mid,str(e))
    @loggedIn
    def movielist(self,to,text,mid):
        r = requests.get("http://www.omdbapi.com/?s="+text+"&apikey=ebd1bd14")
        data = r.text
        data = json.loads(data)
        try:
            if data["Search"] != []:
                no = 0
                a= " 「 Movie 」\nType: Search Movie"
                for music in data["Search"]:
                    no += 1
                    a+= "\n   " + str(no) + ". " + str(music["Title"])
                a+= "\nUsage:%s movie %s|num" %(wait["setkey"], str(text))
                self.sendMessage(to,a)
            else:
                return self.taagsaya(to,"Type: Search Movie",mid,str(text)+" not found")
        except Exception as e:
            return self.taagsaya(to,"Type: Search Movie",mid,"Movie not found!")
    @loggedIn
    def wikipedialistdata(self,to,text,mid,pl=''):
        wiki = WikiApi()
        wiki = WikiApi({'locale' : 'id'})
        start = time.time()
        result = wiki.find(text)
        elapsed_time = time.time() - start
        try:
            article = wiki.get_article(result[pl - 1])
            a = " 「 Wikipedia 」\nType: Search Wiki\nTitle: " + article.heading
            a += "\n\nSummary:\n" + article.summary
            a += "\nLink: " + article.url+"\n"
            self.jangantag(to,' 「 Wikipedia 」','Type: Search Wiki',mid,"Request: Get Wikipedia "+text)
            try:
                self.sendImageWithURL(to,article.image)
            except:
                pass
            self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/4),mid,"Status: Success")
        except Exception as e:
            return self.taagsaya(to,"Type: Search Wiki",mid,str(e))
    @loggedIn
    def youtubelistdata(self,to,text,mid,pl=''):
        r = requests.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q="+text+"&type=video&key=AIzaSyAF-_5PLCt8DwhYc7LBskesUnsm1gFHSP8")
        data = r.text
        data = json.loads(data)
        adit = str(data["items"][pl-1]["id"]['videoId'])
        path = "http://api.ntcorp.us/yt/thumbnail?vid=" + adit
        try:
            start = time.time()
            w = requests.get('https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id='+adit+'&key=AIzaSyAF-_5PLCt8DwhYc7LBskesUnsm1gFHSP8')
            data1 = w.text
            data1 = json.loads(data1)
            elapsed_time = time.time() - start
            for mulai in data1["items"]:
                a= " 「 Youtube 」\nType: Youtube Video\n"
                a+= "\n   Title: " + mulai['snippet']['title']
                a+= "\n   Channel: " + mulai['snippet']['channelTitle']
                a+= "\n   Viewers: " + mulai['statistics']['viewCount']
                a+= "\n   Like: " + mulai['statistics']['likeCount']
                a+= "\n   Comment: " + mulai['statistics']['commentCount']
                a+= "\n\nDescription: " + mulai['snippet']['description']
                a+= "\nLink: youtu.be/"+adit+"\n"
            self.jangantag(to,' 「 Youtube 」','Type: Search Youtube Video',mid,"Request: Get Youtube "+text)
            try:
                self.sendImageWithURL(to,path)
            except:
                pass
            self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/4),mid,"Status: Success")
        except Exception as e:
            return self.taagsaya(to,"Type: Youtube Video",mid,str(e))
    @loggedIn
    def getGroupPostdetail(self,to,text,pl=''):
        data = self.getGroupPost(to)
        try:
            music = data['result']['feeds'][pl - 1]
            try:
                c = str(music['post']['userInfo']['nickname'])
            except:
                c = "Tidak Diketahui"
            a ="\n   Penulis : "+c
            try:
                g= str(music['post']['contents']['text'])
            except:
                g="None"
            a +="\n   Total Like: "+str(music['post']['postInfo']['likeCount'])
            a +="\n   Total Comment: "+str(music['post']['postInfo']['commentCount'])
            gtime = music['post']['postInfo']['createdTime']
            a +="\n   Created at: "+str(timeago.format(datetime.now(),gtime/1000))
            a +="\n\nDescription: "+g
            try:
                for c in music['post']['contents']['media']:
                    params = {'userMid': mid, 'oid': c['objectId']}
                    path = self.server.urlEncode(self.server.LINE_OBS_DOMAIN, '/myhome/h/download.nhn', params)
                self.sendMessage(to," 「 Groups 」\nType: Get Note"+a)
                if c['type'] == 'PHOTO':
                    self.sendImageWithURL(to,path)
                else:
                    pass
                if c['type'] == 'VIDEO':
                    self.sendVideoWithURL(to,path)
                else:
                    pass
            except:
                self.sendMessage(to," 「 Groups 」\nType: Get Note"+a)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def urbansearchdata(self,to,text,mid,pl=''):
        start = time.time()
        r = requests.get("http://api.urbandictionary.com/v0/define?term="+text)
        data = r.text
        data = json.loads(data)
        elapsed_time = time.time() - start
        try:
            music = data['list'][pl - 1]
            a=" 「 Urban 」\nType: Urban Definition"
            a += "\n   Title: "+ str(music['word'])
            a += "\n   Vote: "+str(music['thumbs_up'])
            a += "\n   Author: "+str(music['author'])
            a += "\n   Link: "+str(music['permalink'])
            a += "\n\nDictionary:\n"+str(music['definition'])+"\n"
            self.jangantag(to,' 「 Urban 」\nStatus: Waiting...','Type: Search Definition',mid,"Request: Get Urban "+text)
            self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/2),mid,"Status: Success")
        except Exception as e:
            return self.taagsaya(to,"Type: Search Definition",mid,str(e))
    @loggedIn
    def urbansearch(self,to,text,mid):
        r = requests.get("http://api.urbandictionary.com/v0/define?term="+text)
        data = r.text
        data = json.loads(data)
        if data['list'] != []:
            no = 0
            a = " 「 Urban 」\nType: Urban Definition"
            for music in data['list']:
                no += 1
                a += "\n   " + str(no) + ". " + str(music['word'])
            a += "\nUsage:%s urban %s|num" %(wait["setkey"], text)
            return self.sendMessage(to,a)
        else:
            return self.taagsaya(to,"Type: Search Definition",mid,str(text)+" not found")
    @loggedIn
    def igsearch(self,to,text,mid):
        try:
            r = requests.get("https://www.instagram.com/"+text+"?__a=1")
            data = r.json()
            a=" 「 Instagram 」\nType: Search User Instagram"
            a+="\n   Name : "+str(data['user']['full_name'])
            a+="\n   Biography :\n   "+str(data['user']['biography'])
            a+="\n   Follower : "+str(data['user']['followed_by']['count'])
            a+="\n   Following : "+str(data['user']['follows']['count'])
            a+="\n   Media : "+str(data['user']['media']['count'])
            a+="\n   Verified : "+str(data['user']['is_verified'])
            a+="\n   Private : "+str(data['user']['is_private'])
            a+="\n   Username : "+str(data['user']['username'])
            a+= "\nUsage:%s instagram %s|num" %(wait["setkey"], str(text))
            self.sendImageWithURL(to,data['user']['profile_pic_url_hd'])
            self.sendMessage(to,a)
        except Exception as e:
            return self.taagsaya(to,"Type: Search User Instagram",mid,str(e))
    @loggedIn
    def igsearchdata(self,to,text,mid,pl=''):
        try:
            r = requests.get("https://www.instagram.com/"+text+"?__a=1")
            data = r.json()
            music = data['user']['media']['nodes'][pl - 1]
            gtime=music['date']
            if data['user']['is_private'] == True:
                self.taagsaya(to,"Type: Search User Post",mid,"Private Account")
            else:
                self.jangantag(to,' 「 Instagram 」\nStatus: Waiting...','Type: User Post',mid,"Request: Get Post "+text)
                if music['is_video']:
                    try:
                        b="Caption:\n"+str(music['caption'])
                    except:
                        b="Caption: None"
                    page = 'https://www.instagram.com/p/' + music['code']
                    r = requests.session().get(page)
                    url = re.search(r'"video_url": "([^"]+)"', r.text).group(1)
                    a=" 「 Instagram 」\n"
                    a+="Post type: Video\n"+b
                    a+="\nViews : "+str(music['video_views'])
                    a+="\nLikes : "+str(music['likes']['count'])
                    a+="\nComments : "+str(music['comments']['count'])
                    a+="\nCreated at: "+str(timeago.format(datetime.now(),gtime))
                    a+="\nBy : "+str(data['user']['username'])
                    a+="\nLink: https://www.instagram.com/p/"+str(music['code'])+"\n"
                    start = time.time()
                    self.sendVideoWithURL(to, url)
                    elapsed_time = time.time() - start
                    self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/2),mid,"Status: Success")
                else:
                    try:
                        b="Caption:\n"+str(music['caption'])
                    except:
                        b="Caption: None"
                    a=" 「 Instagram 」\n"
                    a+="Post type: Image\n"+b
                    a+="\nLikes : "+str(music['likes']['count'])
                    a+="\nComments : "+str(music['comments']['count'])
                    a+="\nCreated at: "+str(timeago.format(datetime.now(),gtime))
                    a+="\nBy : "+str(data['user']['username'])
                    a+="\nLink: https://www.instagram.com/p/"+str(music['code'])+"\n"
                    start = time.time()
                    self.sendImageWithURL(to,music['display_src'])
                    elapsed_time = time.time() - start
                    self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/2),mid,"Status: Success")
        except Exception as e:
            return self.taagsaya(to,"Type: Search User Post",mid,str(e))
    @loggedIn
    def musicsearch(self,to,text,mid):
        r = requests.get("http://api.ntcorp.us/joox/search?q="+text)
        data = r.text
        data = json.loads(data)
        if data['result'] != []:
            no = 0
            a = " 「 Music 」\nType: Music List"
            for music in data['result']:
                no += 1
                a += "\n   " + str(no) + ". " + str(music["single"]) + " by " + str(music["artist"])
            a += "\nUsage:%s music %s|num" %(wait["setkey"], str(text))
            return self.sendMessage(to,a)
        else:
            return self.taagsaya(to,"Type: Search Definition",mid,str(text)+" not found")
    @loggedIn
    def musicsearchdata(self,to,text,mid,pl=''):
        start = time.time()
        r = requests.get("http://api.ntcorp.us/joox/search?q="+text)
        data = r.text
        data = json.loads(data)
        elapsed_time = time.time() - start
        try:
            music = data["result"][pl - 1]
            a = " 「 Music 」\nType: Search Music"
            a += "\n  Single : " + str(music["single"])
            a += "\n  Artist : " + str(music["artist"])
            a += "\n  Album  : " + str(music["album"])
            a += "\n  Played : " + str(music["played"])+'\n'
            self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/2),mid,"Status: Success")
            self.jangantag(to,' 「 Music 」\nStatus: Waiting...','Type: Downloading Music',mid,"Request: Get Music "+text)
            path = "http://api.ntcorp.us/joox/d/mp3/" + str(music["sid"])
            self.sendAudioWithURL(to,path)
        except Exception as e:
            return self.taagsaya(to,"Type: Search Music",mid,str(e))
    @loggedIn
    def movielistdata(self,to,text,mid,pl=''):
        start = time.time()
        r = requests.get("http://www.omdbapi.com/?s="+text+"&apikey=ebd1bd14")
        data = r.text
        data = json.loads(data)
        elapsed_time = time.time() - start
        try:
            adit = str(data["Search"][pl - 1]["imdbID"])
            w = requests.get("http://www.omdbapi.com/?i="+adit+"&apikey=ebd1bd14")
            data1 = w.text
            data1 = json.loads(data1)
            a = " 「 Movie 」\nType: Movie List\n   Title: " + str(data1["Title"])
            a += "\n   Release: "+str(data1['Released'])
            a+="\n   Genre: "+str(data1['Genre'])
            a+="\n   Director: "+str(data1['Director'])
            a+="\n   Actors: "+str(data1['Actors'])
            a+="\n   Plot: "+str(data1['Plot'])+"\n"
            self.jangantag(to,' 「 Movie 」\nStatus: Waiting...','Type: Search Movie',mid,"Request: Get Movie "+text)
            try:
                self.sendImageWithURL(to,str(data1["Poster"]))
            except:
                pass
            self.jangantag(to,a,'Taken: %.10f' % (elapsed_time/2),mid,"Status: Success")
        except Exception as e:
            return self.taagsaya(to,"Type: Search Movie",mid,str(e))
    @loggedIn
    def wikipedialist(self,to,text,mid):
        wiki = WikiApi()
        wiki = WikiApi({'locale' : 'id'})
        result = wiki.find(text)
        if result != []:
            no = 0
            a= " 「 Wikipedia 」\nType: Search Wiki"
            for music in result:
                no += 1
                a+= "\n   " + str(no) + ". " + music
            a+= "\nUsage:%s wikipedia %s|num" %(wait["setkey"], str(text))
            self.sendMessage(to,a)
        else:
            return self.taagsaya(to,"Type: Search Wiki",mid,str(text)+" not found")
    @loggedIn
    def stalkerin(self,to,text):
        try:
            contact = self.findContactsByUserid(text)
            self.sendContact(to,contact.mid)
        except:
            return self.sendContact(to,text)
    @loggedIn
    def jadwalshalat(self,to,text):
        r = requests.get("https://time.siswadi.com/pray/?address="+text)
        data = r.text
        data = json.loads(data)
        no = 0
        a= " 「 Pray Time 」\nType: Jadwal Shalat\nTanggal: "+data['time']['date']
        a+= "\n  - Shubuh: "+data['data']['Fajr']+" AM"
        a+= "\n  - Dzuhur: "+data['data']['Dhuhr']+" PM"
        a+= "\n  - Ashar: "+data['data']['Asr']+" PM"
        a+= "\n  - Maghrib: "+data['data']['Maghrib']+" PM"
        a+= "\n  - Isya: "+data['data']['Isha']+" PM"
        return self.sendMessage(to,a)
    @loggedIn
    def youtubelist(self,to,text,mid):
        r = requests.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q="+text+"&type=video&key=AIzaSyAF-_5PLCt8DwhYc7LBskesUnsm1gFHSP8")
        data = r.text
        data = json.loads(data)
        if data["items"] != []:
            no = 0
            ret_ = " 「 Youtube 」\nType: Youtube Video"
            for music in data["items"]:
                no += 1
                ret_ += "\n   " + str(no) + ". " + str(music['snippet']['title'])
            ret_ += "\nUsage: %syoutube %s|num" %(wait["setkey"], text)
            return self.sendMessage(to,ret_)
        else:
            return self.taagsaya(to,"Type: Search Youtube Video",mid,str(text)+" not found")
    @loggedIn
    def google_url_shorten(self,url):
        req_url = 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAzrJV41pMMDFUVPU0wRLtxlbEU-UkHMcI'
        payload = {'longUrl': url}
        headers = {'content-type': 'application/json'}
        r = requests.post(req_url, data=json.dumps(payload), headers=headers)
        resp = json.loads(r.text)
        return resp['id'].replace("https://","")
    @loggedIn
    def memelist(self,to):
        r = requests.get("https://api.imgflip.com/get_memes")
        data = r.text
        data = json.loads(data)
        no = 0
        ret_ = " 「 Meme 」"
        for music in data['data']['memes']:
            no += 1
            ret_ += "\n   " + str(no) + ". " + str(music['name'])
        ret_ += "\nType %smeme |txt|txt|num" %(wait["setkey"])
        return self.sendMessage(to,ret_)
    @loggedIn
    def outall(self):
        gid = self.getGroupIdsJoined()
        for i in gid:
            return self.leaveGroup(i)
    @loggedIn
    def listgroup(self,to,text):
        gid = self.getGroupIdsJoined()
        ret = " 「 Groups 」\nType: List Groups"
        no = 0
        total = len(gid)
        for group in gid:
            G = self.getGroup(group)
            member = len(G.kitsunemembers)
            no += 1
            ret += "\n   " + str(no) + ". " + G.name
        return self.sendMessage(to,ret+"\nTotal Groups: "+str(total))
    @loggedIn
    def giftmessage(self,to):
        a = ("5","7","6","8")
        b = random.choice(a)
        return self.sendMessage(to, text=None, contentMetadata={'PRDTYPE': 'STICKER','STKVER': '1','MSGTPL': b,'STKPKGID': '1380280'}, contentType=9)
    @loggedIn
    def crash(self,to):
        return self.sendContact(to,"cb91ffef906399940d96a02b75021adc1',")
    @loggedIn
    def closeqr(self,to,text):
        try:
            group = self.getGroup(to)
            group.kitsuneTicket = True
            return self.updateGroup(group)
        except:
            return self.sendMessage(to,"It can not be used outside the group")
    @loggedIn
    def urlqr(self,to):
        try:
            g = self.getGroup(to)
            gurl = self.reissueGroupTicket(to)
            self.sendMessage(to,"line://ti/g/" + gurl)
            if g.kitsuneTicket == True:
                g.kitsuneTicket = False
                self.updateGroup(g)
        except:
            return self.sendMessage(to,"It can not be used outside the group")
    @loggedIn
    def openqr(self,to,text):
        try:
            group = self.getGroup(to)
            group.kitsuneTicket = False
            return self.updateGroup(group)
        except:
            return self.sendMessage(to,"It can not be used outside the group")
    @loggedIn
    def cancelinvite(self,to,text):
        group = self.getGroup(to)
        if group.invitee is not None:
            gInviMids = [contact.mid for contact in group.invitee]
            return self.cancelGroupInvitation(to, gInviMids)
    @loggedIn
    def listfriend(self,to,text):
        kitsunefriend = self.getAllContactIds()
        return self.blockmention(to,'',kitsunefriend)
    @loggedIn
    def taagsaya(self,to,text,mid,pl):
        zx = ""
        zxc = " 「 Error 」\nStatus: Requests Error\nFrom: "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2 + "\n"+text+"\nReason: "+pl
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, zxc, contentMetadata)
    @loggedIn
    def jangantag(self,to,pp,text,mid,pl):
        zx = ""
        zxc = pp+"\nFrom: "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2 + "\n"+text+"\n"+pl
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        try:
            self.sendMessage(to, zxc, contentMetadata)
        except:
            return self.sendMessage(to, pp+"\nFrom: "+self.getContact(mid).kitsuneName + "\n"+text+"\n"+pl)
    @loggedIn
    def responsename(self,to):
        zx = ""
        zxc = " 「 Responsename 」\n・ "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':self.profile.mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def autorespontag(self,to,mid):
        zx = ""
        zxc = " 「 Auto Respon 」\n・ "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, zxc, contentMetadata)
    @loggedIn
    def blinwl(self,to,text,mid):
        zx = ""
        zxc = " 「 Blacklist 」\nType: Add Blacklist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" Whitelist User"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def bltobl(self,to,text,mid):
        zx = ""
        zxc = " 「 Blacklist 」\nType: Add Blacklist♪\nStatus: Success\nAdded "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" to black list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def blinbl(self,to,text,mid):
        zx = ""
        zxc = " 「 Blacklist 」\nType: Add Blacklist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" already in black list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mimictoml(self,to,text,mid):
        zx = ""
        zxc = " 「 Mimic 」\nType: Add Mimic♪\nStatus: Success\nAdded "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" to mimic list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mimicinml(self,to,text,mid):
        zx = ""
        zxc = " 「 Mimic 」\nType: Add Mimic♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" already in mimic list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delinbl(self,to,text,mid):
        zx = ""
        zxc = " 「 Blacklist 」\nType: Delete Blacklist♪\nStatus: Success\n "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" delete from black list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delnobl(self,to,text,mid):
        zx = ""
        zxc = " 「 Blacklist 」\nType: Delete Blacklist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" nothing in black list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delinml(self,to,text,mid):
        zx = ""
        zxc = " 「 Mimic 」\nType: Delete Mimic♪\nStatus: Success\n "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" delete from mimic list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delnoml(self,to,text,mid):
        zx = ""
        zxc = " 「 Mimic 」\nType: Delete Mimic♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" nothing in mimic list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def contactsa(self,to,text,mid):
        a = self.getContact(mid)
        zx = ""
        zxc = " 「 Contact 」\nName: "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc + "\n\nMid: "+mid+"\n\nStatus Message:\n"+a.kitsuneBio
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def wlinbl(self,to,text,mid):
        zx = ""
        zxc = " 「 Whitelist 」\nType: Add Whitelist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" Blacklist User"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def wltowl(self,to,text,mid):
        zx = ""
        zxc = " 「 Whitelist 」\nType: Add Whitelist♪\nStatus: Success\nAdded "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" to white list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def wlinwl(self,to,text,mid):
        zx = ""
        zxc = " 「 Whitelist 」\nType: Add Whitelist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" already in white list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delinwl(self,to,text,mid):
        zx = ""
        zxc = " 「 Whitelist 」\nType: Delete Whitelist♪\nStatus: Success\n "
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" delete from white list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def delnowl(self,to,text,mid):
        zx = ""
        zxc = " 「 Whitelist 」\nType: Delete Whitelist♪\nStatus: Error\n"
        zx2 = []
        pesan2 = "@a"" "
        xlen = str(len(zxc))
        xlen2 = str(len(zxc)+len(pesan2)-1)
        zx = {'S':xlen, 'E':xlen2, 'M':mid}
        zx2.append(zx)
        zxc += pesan2
        text = zxc +" nothing in white list"
        contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def sendContact(self,to,mid):
        admsa = mid
        return self.sendMessage(to, text=None, contentMetadata={'mid': admsa}, contentType=13)
    @loggedIn
    def blmention(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Blacklist 」\nBlack List: %i Target' % len(dataMid)
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nCommand:\n- Add blacklist\n   Usage:"+wait["setkey"].title()+" addbl [@|~|on|repeat]\n- Delete blacklist\n   Usage:"+wait["setkey"].title()+" delbl [@|~|on|repeat]\n- Clear blacklist\n   Usage:"+wait["setkey"].title()+" clearbl"
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    def wlmention(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Whitelist 」\nWhite List: %i Target' % len(dataMid)
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nCommand:\n- Add whitelist\n   Usage:"+wait["setkey"].title()+" addwl [@|~|on]\n- Delete whitelist\n   Usage:"+wait["setkey"].title()+" delwl [@|~|on]\n- Clear whitelist\n   Usage:"+wait["setkey"].title()+" clearwl"
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    def wlmention2(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Whitelist 」\nWhite List:'
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=150
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nCommand:\n- Add whitelist\n   Usage:"+wait["setkey"].title()+" addwl [@|~|on]\n- Delete whitelist\n   Usage:"+wait["setkey"].title()+" delwl [@|~|on]\n- Clear whitelist\n   Usage:"+wait["setkey"].title()+" clearwl"
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mcmention(self, to, text='', dataMid=[]):
        arr = []
        list_text='Mimic: %i Target' % len(dataMid)
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nCommand:\n- Add target\n   Usage:"+wait["setkey"].title()+" add mimic [@|~]\n- Delete target\n   Usage:"+wait["setkey"].title()+" del mimic [@|~]\n- ON/OFF target\n   Usage:"+wait["setkey"].title()+" mimic [on|off][@]"
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def sidertag1(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Lurking 」\nLurkers: %i Orang'%(len(dataMid))
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=150
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text +="\nData Dicatat:\n"+datetime.now().strftime('%H:%M:%S')
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def blmention2(self, to, text='', dataMid=[]):
        arr = []
        list_text=''
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=150
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nCommand:\n- Add blacklist\n   Usage:"+wait["setkey"].title()+" addbl [@|~|on|repeat]\n- Delete blacklist\n   Usage:"+wait["setkey"].title()+" delbl [@|~|on|repeat]\n- Clear blacklist\n   Usage:"+wait["setkey"].title()+" clearbl"
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def blmention1(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Blacklist 」\nBlack List: %i Target' % len(wait["blacklist"])
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def wlmention1(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Whitelist 」\nWhite List: %i Target' % len(wait["bots"])
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n   '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mention1(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Mention 」 \nType: Tag'
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n  '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nStatus: Success\nTotal: %i Target"%(len(dataMid))
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mention12(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Mention 」 \nType: Tag'
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=0
            for l in dataMid:
                no+=1
                list_text+='\n  '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mention3(self, to, text='', dataMid=[]):
        arr = []
        list_text=' 「 Mention 」 \nType: Tag'
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=300
            for l in dataMid:
                no+=1
                list_text+='\n  '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nStatus: Success\nTotal: %i Target"%(len(dataMid)+300)
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def mention2(self, to, text='', dataMid=[]):
        arr = []
        list_text=''
        if '[list]' in text.lower():
            i=0
            for l in dataMid:
                list_text+='\n@[list-'+str(i)+']'
                i=i+1
            text=text.replace('[list]', list_text)
        elif '[list-' in text.lower():
            text=text
        else:
            i=0
            no=150
            for l in dataMid:
                no+=1
                list_text+='\n  '+str(no)+'. @[list-'+str(i)+']'
                i=i+1
            list_text+="\nStatus: Success\nTotal: %i Target"%(len(dataMid)+150)
            text=text+list_text
        i=0
        for l in dataMid:
            mid=l
            name='@[list-'+str(i)+']'
            ln_text=text.replace('\n',' ')
            if ln_text.find(name):
                line_s=int( ln_text.index(name) )
                line_e=(int(line_s)+int( len(name) ))
            arrData={'S': str(line_s), 'E': str(line_e), 'M': mid}
            arr.append(arrData)
            i=i+1
        contentMetadata={'MENTION':str('{"MENTIONEES":' + json.dumps(arr).replace(' ','') + '}')}
        return self.sendMessage(to, text, contentMetadata)
    @loggedIn
    def listmember(self,to):
        group = self.getGroup(to)
        zxc = " 「 List Member 」\nName:\n"+group.name+"\n\nMembers:"
        total = "\n\nGroup ID:\n"+to
        no = 0
        if len(group.kitsunemembers) > 0:
            for a in group.kitsunemembers:
                no += 1
                zxc += "\n" + str(no) + ". " + a.kitsuneName
        return self.sendMessage(to,zxc+total)
    @loggedIn
    def backupmyprofile(self,to):
        h = open('name.txt',"r")
        name = h.read()
        h.close()
        x = name
        self.getProfile().kitsuneName = x
        self.updateProfile(self.getProfile())
        i = open('stat.txt',"r")
        sm = i.read()
        i.close()
        y = sm
        self.getProfile().kitsuneBio = y
        self.updateProfile(self.getProfile())
        j = open('photo.txt',"r")
        ps = j.read()
        j.close()
        p = ps
        self.updateProfileAttribute(8, p)
        self.sendMessage(to," 「 Backup Profil 」\nSukses Backup\nDisplayName:" + self.getProfile().kitsuneName + "\n「Status 」\n" + self.getProfile().kitsuneBio)
        try:
            self.sendImageWithURL(to,"http://dl.profile.line-cdn.net/" + self.getProfile().picturePath)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def GroupPost(self,to):
        data = self.getGroupPost(to)
        if data['result'] != []:
            try:
                no = 0
                a = " 「 Groups 」\nType: Get Note"
                for i in data['result']['feeds']:
                    no += 1
                    gtime = i['post']['postInfo']['createdTime']
                    try:
                        c = str(i['post']['userInfo']['nickname'])
                    except:
                        c = "Tidak Diketahui"
                    a +="\n   Penulis : "+c
                    try:
                        g= str(i['post']['contents']['text'])
                    except:
                        g="None"
                    a +="\n   Description: "+g
                    a +="\n   Total Like: "+str(i['post']['postInfo']['likeCount'])
                    a +="\n   Created at: "+str(timeago.format(datetime.now(),gtime/1000)) + "\n"
                a +="Status: Success Get "+str(data['result']['homeInfo']['postCount'])+" Note"
                a += "\nUsage:%s get note num" %(wait["setkey"])
                self.sendMessage(to,a)
            except Exception as e:
                return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def speed(self,to,text):
        start = time.time()
        self.sendMessage(to, " 「 Speed 」\nType: Speed\nTesting..")
        elapsed_time = time.time() - start
        took = time.time()- start
        self.sendMessage(to," 「 Speed 」\nType: Speed\n - Took : %.3fms\n - Taken: %.10f" % (took/4,elapsed_time/4))
    @loggedIn
    def stackspeed(self,to,text):
        start = time.time()
        print(start)
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
        elapsed_time = time.time() - start
        self.sendMessage(to,"Taken: %.10f" % (elapsed_time/4))
    @loggedIn
    def ikkehtarget(self,to,mid):
        self.clone(mid)
        group = self.getContact(mid)
        contact = "http://dl.profile.line-cdn.net/" + group.kitsunephotoStatus
        try:
            zx = ""
            zxc = " 「 Copy Profile 」\n- Target: ・"
            zx2 = []
            pesan2 = "@a"" "
            xlen = str(len(zxc))
            xlen2 = str(len(zxc)+len(pesan2)-1)
            zx = {'S':xlen, 'E':xlen2, 'M':mid}
            zx2.append(zx)
            zxc += pesan2
            zxc +="\n- Status: Success Copy profile♪"
            text = zxc
            contentMetadata = {'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}
            self.sendMessage(to, text, contentMetadata)
            self.sendImageWithURL(to,contact)
        except Exception as e:
            return self.sendMessage(to,"「 Auto Respond 」\n"+str(e))
    @loggedIn
    def myticket(self,to):
        ticket = self.getUserTicket()
        userid = ticket.id
        return self.sendMessage(to," 「 User Ticket 」\nTicket : line://ti/p/"+str(userid))
    @loggedIn
    def removeMessage(self, messageId):
        return self._client.removeMessage(messageId)
        
    @loggedIn
    def removeAllMessages(self, lastMessageId):
        return self._client.removeAllMessages(0, lastMessageId)
        
    @loggedIn
    def sendChatChecked(self, consumer, messageId):
        return self._client.sendChatChecked(0, consumer, messageId)

    @loggedIn
    def sendEvent(self, messageObject):
        return self._client.sendEvent(0, messageObject)

    @loggedIn
    def getLastReadMessageIds(self, chatId):
        return self._client.getLastReadMessageIds(0,chatId)

    """Contact"""
        
    @loggedIn
    def blockContact(self, mid):
        return self._client.blockContact(0, mid)

    @loggedIn
    def unblockContact(self, mid):
        return self._client.unblockContact(0, mid)

    @loggedIn
    def findAndAddContactsByMid(self, mid):
        return self._client.findAndAddContactsByMid(0, mid)

    @loggedIn
    def findAndAddContactsByUserid(self, userid):
        return self._client.findAndAddContactsByUserid(0, userid)

    @loggedIn
    def findContactsByUserid(self, userid):
        return self._client.findContactByUserid(userid)

    @loggedIn
    def findContactByTicket(self, ticketId):
        return self._client.findContactByUserTicket(ticketId)

    @loggedIn
    def getAllContactIds(self):
        return self._client.getAllContactIds()

    @loggedIn
    def getBlockedContactIds(self):
        return self._client.getBlockedContactIds()

    @loggedIn
    def getContact(self, mid):
        return self._client.getContact(mid)

    @loggedIn
    def getContacts(self, midlist):
        return self._client.getContacts(midlist)

    @loggedIn
    def getFavoriteMids(self):
        return self._client.getFavoriteMids()

    @loggedIn
    def getHiddenContactMids(self):
        return self._client.getHiddenContactMids()

    @loggedIn
    def reissueUserTicket(self, expirationTime=100, maxUseCount=100):
        return self._client.reissueUserTicket(expirationTime, maxUseCount)
    
    @loggedIn
    def clone(self, mid):
        contact = self.getContact(mid)
        profile = self.profile
        profile.kitsuneBio = contact.kitsuneBio
        return self.updateProfile(profile)

    """Group"""
    
    @loggedIn
    def findGroupByTicket(self, ticketId):
        return self._client.findGroupByTicket(ticketId)

    @loggedIn
    def acceptGroupInvitation(self, groupId):
        return self._client.acceptGroupInvitation(0, groupId)

    @loggedIn
    def acceptGroupInvitationByTicket(self, groupId, ticketId):
        return self._client.acceptGroupInvitationByTicket(0, groupId, ticketId)

    @loggedIn
    def cancelGroupInvitation(self, groupId, contactIds):
        return self._client.cancelGroupInvitation(0, groupId, contactIds)

    @loggedIn
    def createGroup(self, name, midlist):
        return self._client.createGroup(0, name, midlist)

    @loggedIn
    def getGroup(self, groupId):
        return self._client.getGroup(groupId)

    @loggedIn
    def getGroups(self, groupIds):
        return self._client.getGroups(groupIds)

    @loggedIn
    def getGroupIdsInvited(self):
        return self._client.getGroupIdsInvited()

    @loggedIn
    def getGroupIdsJoined(self):
        return self._client.getGroupIdsJoined()

    @loggedIn
    def inviteIntoGroup(self, groupId, midlist):
        return self._client.inviteIntoGroup(0, groupId, midlist)

    @loggedIn
    def kickoutFromGroup(self, groupId, midlist):
        return self._client.kickoutFromGroup(0, groupId, midlist)

    @loggedIn
    def leaveGroup(self, groupId):
        return self._client.leaveGroup(0, groupId)

    @loggedIn
    def rejectGroupInvitation(self, groupId):
        return self._client.rejectGroupInvitation(0, groupId)

    @loggedIn
    def reissueGroupTicket(self, groupId):
        return self._client.reissueGroupTicket(groupId)

    @loggedIn
    def updateGroup(self, groupObject):
        return self._client.updateGroup(0, groupObject)

    """Room"""

    @loggedIn
    def createRoom(self, midlist):
        return self._client.createRoom(0, midlist)

    @loggedIn
    def getRoom(self, roomId):
        return self._client.getRoom(roomId)

    @loggedIn
    def inviteIntoRoom(self, roomId, midlist):
        return self._client.inviteIntoRoom(0, roomId, midlist)

    @loggedIn
    def leaveRoom(self, roomId):
        return self._client.leaveRoom(0, roomId)

    """Call"""
        
    @loggedIn
    def acquireCallRoute(self, to):
        return self._client.acquireCallRoute(to)